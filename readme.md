 # ListIPs 

ListIPs is a php class for taking a list of IPs and putting them into more usable format. These include such things as cisco blocklists, iptables blocklists, .htaccess deny lists, nginx deny lists, and much more.

## PHP Docs

https://listips.docs.munzy.io/


## Format List

```
$listIPs = new ListIps\`Class`;
```

| Format | Code | Class |
|---|---| --- |
| cisco | `$ListIPs->cisco("AS16");` | cisco | 
| htaccess | `$ListIPs->htaccess();` | htaccess | 
| blackhole | `$ListIPs->blackhole();` | blackhole | 
| blackhole remove | `$ListIPs->blackholeDelete();` | blackhole | 
| ipset | `$ListIPs->ipset("AS16");` | ipset | 
| iptables | `$ListIPs->iptables();` | iptables | 
| list | `$ListIPs->ipList();` | iplist | 
| nginx | `$ListIPs->nginx();` | nginx | 
| raw | `$ListIPs->raw();` | raw |
| tsv | `$listIPs->tsv();` | tsv |
| csv | `$listIPs->csv();` | csv | 
| juniper | `$listIPs->juniper();` | juniper | 


## Import

Import, is the first and most imporant function of ListIPs. Otherwise, you won't be able to use the class.

First, create a list of ips like such. This can be done in string format, or array.

```
"list": [
    "2620:83:8000::\/48",
    "131.243.0.0\/16",
    "128.3.0.0\/16"
]
```
From ASN #16, LBL - Lawrence Berkeley National Laboratory, US.

Now, to import, simply execute: 

` $ListIPs->import($list);`

## Iptables

Now, with the above import done. Let's create an iptables blocklist.
To do this we simply need to execute:

`$ListIPs->iptables()->text();`

This will return:
```
ip6tables -A INPUT -s 2620:83:8000::/48 -j DROP
iptables -A INPUT -s 131.243.0.0/16 -j DROP
iptables -A INPUT -s 128.3.0.0/16 -j DROP
```

## Ipset

Now, follow the same guide and you can easily get ipset lists. However, we add a special naming field so that you can identify your ipset block list easier.

`$ListIPs->ipset("AS16")->text();`

This will return:
```
ipset -N AS16-4 hash:net family inet
ipset -N AS16-6 hash:net family inet6
ipset -A AS16-6 2620:83:8000::/48
ipset -A AS16-4 131.243.0.0/16
ipset -A AS16-4 128.3.0.0/16
iptables -A INPUT -m set --match-set AS16-4 src -j DROP
ip6tables -A INPUT -m set --match-set AS16-6 src -j DROP
```

## JSON

Both of the above listed results in text format. This may be useful for creating the actual lists to install on a server. However, we sometimes want to create an API for other applications to interface with. 

To do this, we simply change the structure of our command to swap text into json. The json() function contains a variable so that you can pass anything you like into the name field of the json response. The JSON function works with all formats in this app.

`$ListIPs->cisco("AS16")->json("LBL - Lawrence Berkeley National Laboratory, US");`
This will return:

```
{
    "name": "LBL - Lawrence Berkeley National Laboratory, US",
    "type": "cisco",
    "list": [
        "object network AS16-6-SN0",
        "subnet 2620:83:8000::\/48",
        "object network AS16-4-SN0",
        "subnet 131.243.0.0 255.255.0.0",
        "object network AS16-4-SN1",
        "subnet 128.3.0.0 255.255.0.0",
        "object-group network AS16",
        "network-object object AS16-6-SN0",
        "network-object object AS16-4-SN0",
        "network-object object AS16-4-SN1"
    ]
}
```

## RAW

Want to see our data in its raw form! Well here you go! 

`$ListIPs->raw()->json("LBL - Lawrence Berkeley National Laboratory, US");`

The response will be like:
```
{
    "name": "LBL - Lawrence Berkeley National Laboratory, US",
    "type": "raw",
    "list": [
        {
            "ip": "2620:83:8000::",
            "subnetMask": false,
            "cidr": "48",
            "type": 6
        },
        {
            "ip": "131.243.0.0",
            "subnetMask": "255.255.0.0",
            "cidr": "16",
            "type": 4
        },
        {
            "ip": "128.3.0.0",
            "subnetMask": "255.255.0.0",
            "cidr": "16",
            "type": 4
        }
    ]
}
```

## Htaccess

htaccess is used in LiteSpeed Web Server and Apache. This will create you an easy to use blocklist in a jiffy!


`$ListIPs->htaccess()->text();`

This will return: 
```
Order Deny,Allow
Deny from 2620:83:8000::/48
Deny from 131.243.0.0/16
Deny from 128.3.0.0/16
```

## Blackhole

IP blackhole is a super efficient way of blocking ranges on a Linux based server. It is by far more effecitent than IPtables. 

`$ListIPs->blackhole()->text();`

This will return:
```
ip route add blackhole 2620:83:8000::/48
ip route add blackhole 131.243.0.0/16
ip route add blackhole 128.3.0.0/16
```

However, once the deed is done it can take a bit of work to remove these blocklists. To help with this, we have made a nice remove list as well.

`$ListIPs->blackholeDelete()->text();`

This will return:
```
ip route del blackhole 2620:83:8000::/48
ip route del blackhole 131.243.0.0/16
ip route del blackhole 128.3.0.0/16
```

## List

Boring Boring Lists... but they do come in handy!

`$ListIPs->list()->json("LBL - Lawrence Berkeley National Laboratory, US");`

This will return:
```
{
    "name": "LBL - Lawrence Berkeley National Laboratory, US",
    "type": "ipList",
    "list": [
        "2620:83:8000::\/48",
        "131.243.0.0\/16",
        "128.3.0.0\/16"
    ]
}
```

## Nginx

Nginx is a super fast web server, and used around the world. However, it is even better at filtering out bad traffic before it hits your backend dynamic and content filters. With ListIPs you can easily create deny lists, and import them into your nginx web servers.

`$ListIPs->nginx()->text();`

This will return: 
```
deny 2620:83:8000::/48;
deny 131.243.0.0/16;
deny 128.3.0.0/16;
```

## TSV & CSV

Tab Seperated Values (tsv), and Comma Sperated Values (csv) are commonly used spreadsheet format. This can be easily imported and modified inside of Microsoft Excel, Google Spreadsheets, and many more.

`$ListIPs->csv()->text();`

This will return: 
```
IP Address,Subnet Mask,CIDR,Type
128.3.0.0,255.255.0.0,16,4
131.243.0.0,255.255.0.0,16,4
2620:83:8000::,FALSE,48,6
177.55.221.0,255.255.255.0,24,4
177.55.220.0,255.255.255.0,24,4
```

`$ListIPs->tsv()->text();`

```
IP Address	Subnet Mask	CIDR	Type
128.3.0.0	255.255.0.0	16	4
131.243.0.0	255.255.0.0	16	4
2620:83:8000::	FALSE	48	6
177.55.221.0	255.255.255.0	24	4
177.55.220.0	255.255.255.0	24	4
```

## Juniper

`$ListIPs->tsv('AS16')->text();`

```
set policy-options prefix-list AS16v6 2620:83:8000::/48
set policy-options prefix-list AS16v4 131.243.0.0/16
set policy-options prefix-list AS16v4 128.3.0.0/16
```

## Other Functions

 Cidr2Mask is a function to quickly determine the mask of a CIDR. For example changing 24 into 255.255.255.0.

`$ListIPs->cidr2mask(24);`

The reverse of this is mask2cidr. Which will change 255.255.255.0 into 24. To do this execute:

`$ListIPs->mask2cidr("255.255.255.0");`

If on the off change you throw an invalid mask inside of this function, it will return false.

If you wish to delete the list of ips inside of the function to import a new list simply run:

`$ListIPs->delete();`

Or if you would like to dump the IPs inside of the class out to see what is inside:

`$ListIPs->dump();`

Want to limit to only IPv4 addresses? We got you covered. Please note, this will remove IPv6 addresses from the list entirely. If you want to add them again, you will need to reimport.

`$listIPs->limit4();`

Care for the next generation IPv6 only, we got you covered too! Please note, this will remove IPv6 addresses from the list entirely. If you want to add them again, you will need to reimport.

`$listIPs->limit6();`


And that is it!

# Donations

If you like my work, please buy me a cup of coffee! 

https://www.cameronmunroe.com/coffee