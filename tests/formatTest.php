<?php

// Load tests.
use PHPUnit\Framework\TestCase;

class formats extends TestCase{

    // We are using University of Alberta AS3359 as testing. 
    // You -SHOULD NOT- use these in your own listings just
    // because we have them listed here.

    public function test_iptables(){

        $list = file_get_contents("./tests/assets/ipList.txt");

        $expected = file_get_contents("./tests/assets/iptables.txt");

        $ListIPs = new \ListIPs\iptables;

        $delivered = $ListIPs->import($list)->iptables()->text();

        $this->assertEquals($expected, $delivered);
    }


    public function test_nginx(){

        $list = file_get_contents("./tests/assets/ipList.txt");

        $expected = file_get_contents("./tests/assets/nginx.conf");

        $ListIPs = new \ListIPs\nginx;

        $delivered = $ListIPs->import($list)->nginx()->text();

        $this->assertEquals($expected, $delivered);
    }


    public function test_ipBlackhole(){

        $list = file_get_contents("./tests/assets/ipList.txt");

        $expected = file_get_contents("./tests/assets/ipBlackhole.txt");

        $ListIPs = new \ListIPs\blackhole;

        $delivered = $ListIPs->import($list)->blackhole()->text();

        $this->assertEquals($expected, $delivered);
    }


    public function test_ipBlackholeRemove(){

        $list = file_get_contents("./tests/assets/ipList.txt");

        $expected = file_get_contents("./tests/assets/ipBlackholeRemove.txt");

        $ListIPs = new \ListIPs\blackhole;

        $delivered = $ListIPs->import($list)->blackholeDelete()->text();

        $this->assertEquals($expected, $delivered);
    }


    public function test_htaccess(){

        $list = file_get_contents("./tests/assets/ipList.txt");

        $expected = file_get_contents("./tests/assets/htaccess.txt");

        $ListIPs = new \ListIPs\htaccess;

        $delivered = $ListIPs->import($list)->htaccess()->text();

        $this->assertEquals($expected, $delivered);
    }


    public function test_ipset(){

        $list = file_get_contents("./tests/assets/ipList.txt");

        $expected = file_get_contents("./tests/assets/ipset.txt");

        $ListIPs = new \ListIPs\ipset;

        $delivered = $ListIPs->import($list)->ipset("AS3359")->text();

        $this->assertEquals($expected, $delivered);
    }


    public function test_ipList(){

        $list = file_get_contents("./tests/assets/ipList.txt");

        $expected = file_get_contents("./tests/assets/ipList.txt");

        $ListIPs = new \ListIPs\iplist;

        $delivered = $ListIPs->import($list)->ipList()->text();

        $this->assertEquals($expected, $delivered);
    }

    public function test_raw_json(){

        $list = file_get_contents("./tests/assets/ipList.txt");

        $ListIPs = new \ListIPs\raw;

        $delivered = $ListIPs->import($list)->raw()->json();

        $delivered = json_decode($delivered, true);

        $expected = array(
            'ip' => "208.75.74.0",
            'subnetMask' => "255.255.255.0",
            'cidr' => 24,
            'type' => 4,
        );

        $this->assertEquals($expected, $delivered['list'][0]);
    }


    public function test_list_json(){

        $list = file_get_contents("./tests/assets/ipList.txt");

        $expected = file_get_contents("./tests/assets/json.txt");

        $ListIPs = new \ListIPs\iplist;

        $delivered = $ListIPs->import($list)->ipList()->json("AS3359");

        $this->assertEquals($expected, $delivered);
    }


    public function test_list_cisco(){

        $list = file_get_contents("./tests/assets/ciscoIPs.txt");

        $expected = file_get_contents("./tests/assets/cisco.txt");

        $ListIPs = new \ListIPs\cisco;

        $delivered = $ListIPs->import($list)->cisco("ASx")->text();

        $this->assertEquals($expected, $delivered);
    }

    public function test_list_juniper(){

        $list = file_get_contents("./tests/assets/ipList.txt");

        $expected = file_get_contents("./tests/assets/juniper.txt");

        $ListIPs = new \ListIPs\juniper;

        $delivered = $ListIPs->import($list)->juniper("AS3359")->text();

        $this->assertEquals($expected, $delivered);
    }

    public function test_list_v4list(){

        $list = file_get_contents("./tests/assets/ipList.txt");

        $expected = file_get_contents("./tests/assets/v4list.txt");

        $ListIPs = new \ListIPs\iplist;

        $delivered = $ListIPs->import($list)->limit4()->ipList()->text();

        $this->assertEquals($expected, $delivered);
    }

        public function test_list_v6list(){

        $list = file_get_contents("./tests/assets/ipList.txt");

        $expected = file_get_contents("./tests/assets/v6list.txt");

        $ListIPs = new \ListIPs\iplist;

        $delivered = $ListIPs->import($list)->limit6()->ipList()->text();

        $this->assertEquals($expected, $delivered);
    }

    public function test_list_tsv(){

        $list = file_get_contents("./tests/assets/ipList.txt");

        $expected = file_get_contents("./tests/assets/raw.tsv");

        $ListIPs = new \ListIPs\tsv;

        $delivered = $ListIPs->import($list)->tsv()->text();

        $this->assertEquals($expected, $delivered);
    }

    public function test_list_csv(){

        $list = file_get_contents("./tests/assets/ipList.txt");

        $expected = file_get_contents("./tests/assets/raw.csv");

        $ListIPs = new \ListIPs\csv;

        $delivered = $ListIPs->import($list)->csv()->text();

        $this->assertEquals($expected, $delivered);
    }
}