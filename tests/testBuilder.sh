#!/bin/bash

apt-get update -qyy >> ./tests/logs.txt

apt-get install zip unzip -qyy >> ./tests/logs.txt

php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" >> ./tests/logs.txt

php composer-setup.php >> ./tests/logs.txt

php composer.phar install >> ./tests/logs.txt

php composer.phar require phpunit/phpunit --dev >> ./tests/logs.txt

