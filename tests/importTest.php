<?php

// Load tests.
use PHPUnit\Framework\TestCase;

class import extends TestCase{

    public function test_import_single_v4(){

        $item = "192.168.1.1";

        $ListIPs = new \ListIPs\core;

        $ListIPs->import($item);

        $response = $ListIPs->dump();

        $this->assertInternalType('array', $response);

        $this->assertInternalType('array', $response[0]);

        $this->assertEquals($response[0]['ip'], $item);

        $this->assertEquals($response[0]['cidr'], 32);

        $this->assertEquals($response[0]['subnetMask'], "255.255.255.255");
        
        $this->assertEquals($response[0]['type'], 4);
        
    }

    public function test_import_single_v6(){

        $item = "2001:480:30::1";

        $ListIPs = new \ListIPs\core;

        $ListIPs->import($item);

        $response = $ListIPs->dump();

        $this->assertInternalType('array', $response);

        $this->assertInternalType('array', $response[0]);

        $this->assertEquals($response[0]['ip'], $item);

        $this->assertEquals($response[0]['cidr'], 128);

        $this->assertEquals($response[0]['subnetMask'], 'ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff');
        
        $this->assertEquals($response[0]['type'], 6);
        
    }

    public function test_import_single_v4_cidr(){

        $item = "192.168.1.0/24";

        $ListIPs = new \ListIPs\core;

        $ListIPs->import($item);

        $response = $ListIPs->dump();

        $this->assertInternalType('array', $response);

        $this->assertInternalType('array', $response[0]);

        $this->assertEquals($response[0]['ip'], "192.168.1.0");

        $this->assertEquals($response[0]['cidr'], 24);

        $this->assertEquals($response[0]['subnetMask'], "255.255.255.0");
        
        $this->assertEquals($response[0]['type'], 4);
        
    }

    public function test_import_single_v4_mask(){

        $item = "192.168.1.0/255.255.255.0";

        $ListIPs = new \ListIPs\core;

        $ListIPs->import($item);

        $response = $ListIPs->dump();

        $this->assertInternalType('array', $response);

        $this->assertInternalType('array', $response[0]);

        $this->assertEquals($response[0]['ip'], "192.168.1.0");

        $this->assertEquals($response[0]['cidr'], 24);

        $this->assertEquals($response[0]['subnetMask'], "255.255.255.0");
        
        $this->assertEquals($response[0]['type'], 4);
        
    }

    public function test_import_single_v4_range(){

        $item = "192.168.1.0-192.168.1.255";

        $ListIPs = new \ListIPs\core;

        $ListIPs->import($item);

        $response = $ListIPs->dump();

        $this->assertInternalType('array', $response);

        $this->assertInternalType('array', $response[0]);

        $this->assertEquals($response[1]['ip'], "192.168.1.1");

        $this->assertEquals($response[1]['cidr'], 32);

        $this->assertEquals($response[1]['subnetMask'], "255.255.255.255");
        
        $this->assertEquals($response[1]['type'], 4);
        
    }

    public function test_import_single_v6_cidr(){

        $item = "2001:480:30::/64";

        $ListIPs = new \ListIPs\core;

        $ListIPs->import($item);

        $response = $ListIPs->dump();

        $this->assertInternalType('array', $response);

        $this->assertInternalType('array', $response[0]);

        $this->assertEquals($response[0]['ip'], "2001:480:30::");

        $this->assertEquals($response[0]['cidr'], 64);

        $this->assertEquals($response[0]['subnetMask'], 'ffff:ffff:ffff:ffff::');
        
        $this->assertEquals($response[0]['type'], 6);
        
    }

    public function test_import_single_v6_than_del(){

        $item = "2001:480:30::/64";

        $ListIPs = new \ListIPs\core;

        $ListIPs->import($item);

        $response = $ListIPs->dump();

        $this->assertInternalType('array', $response);

        $this->assertInternalType('array', $response[0]);

        $this->assertEquals($response[0]['ip'], "2001:480:30::");

        $this->assertEquals($response[0]['cidr'], 64);

        $this->assertEquals($response[0]['subnetMask'], 'ffff:ffff:ffff:ffff::');
        
        $this->assertEquals($response[0]['type'], 6);

        $response = $ListIPs->delete()->dump();

        $this->assertEquals($response[0]['ip'], null);

    }


    public function test_import_random(){

        $list = array();
        $list[] = "2001:480:30::/64";
        $list[] = "192.168.1.1-192.168.1.3";

        $ListIPs = new \ListIPs\core;

        $ListIPs->import($list);

        $response = $ListIPs->dump();

        $this->assertInternalType('array', $response);

        $this->assertInternalType('array', $response[0]);

        $this->assertEquals($response[0]['ip'], "2001:480:30::");

        $this->assertEquals($response[0]['cidr'], 64);

        $this->assertEquals($response[0]['subnetMask'], 'ffff:ffff:ffff:ffff::');
        
        $this->assertEquals($response[0]['type'], 6);

        $this->assertEquals($response[1]['ip'], "192.168.1.1");

        $this->assertEquals($response[2]['ip'], "192.168.1.2");

        $this->assertEquals($response[3]['ip'], "192.168.1.3");

        $this->assertEquals($response[4]['ip'], null);


    }

}
