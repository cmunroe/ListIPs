<?php

// Load tests.
use PHPUnit\Framework\TestCase;

class cidrTest extends TestCase{

    public function testcidr_1(){
    
        $ListIPS = new \ListIPs\core;
 
        $cidr = -1;
 
        $this->assertFalse($ListIPS->cidr2mask($cidr));
 
    }

    public function testcidr8(){

        $ListIPS = new \ListIPs\core;

        $cidr = 8;

        $this->assertEquals($ListIPS->cidr2mask($cidr), "255.0.0.0");

    }

    public function testcidr9(){

        $ListIPS = new \ListIPs\core;

        $cidr = 9;

        $this->assertEquals($ListIPS->cidr2mask($cidr), "255.128.0.0");

    }

    public function testcidr10(){

        $ListIPS = new \ListIPs\core;

        $cidr = 10;

        $this->assertEquals($ListIPS->cidr2mask($cidr), "255.192.0.0");

    }

    public function testcidr11(){

        $ListIPS = new \ListIPs\core;

        $cidr = 11;

        $expected = "255.224.0.0";

        $this->assertEquals($ListIPS->cidr2mask($cidr), $expected);

    }

    public function testcidr12(){

        $ListIPS = new \ListIPs\core;

        $cidr = 12;

        $expected = "255.240.0.0";

        $this->assertEquals($ListIPS->cidr2mask($cidr), $expected);

    }

    public function testcidr13(){

        $ListIPS = new \ListIPs\core;

        $cidr = 13;

        $expected = "255.248.0.0";

        $this->assertEquals($ListIPS->cidr2mask($cidr), $expected);

    }
    public function testcidr14(){

        $ListIPS = new \ListIPs\core;

        $cidr = 14;

        $expected = "255.252.0.0";

        $this->assertEquals($ListIPS->cidr2mask($cidr), $expected);

    }
    public function testcidr15(){

        $ListIPS = new \ListIPs\core;

        $cidr = 15;

        $expected = "255.254.0.0";

        $this->assertEquals($ListIPS->cidr2mask($cidr), $expected);

    }

    public function testcidr16(){

        $ListIPS = new \ListIPs\core;

        $cidr = 16;

        $this->assertEquals($ListIPS->cidr2mask($cidr), "255.255.0.0");

    }

    public function testcidr33(){

        $ListIPS = new \ListIPs\core;

        $cidr = 33;

        $this->assertFalse($ListIPS->cidr2mask($cidr));

    }

    public function testcidrword(){

        $ListIPS = new \ListIPs\core;

        $cidr = "a";

        $this->assertFalse($ListIPS->cidr2mask($cidr));

    }

    public function testcidrSpace(){

        $ListIPS = new \ListIPs\core;

        $cidr = " 16 ";

        $mask = "255.255.0.0";

        $this->assertEquals($ListIPS->cidr2mask($cidr), $mask);

    }

}