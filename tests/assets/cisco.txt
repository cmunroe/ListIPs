object network ASx-4-SN0
subnet 5.196.0.0 255.255.0.0
object network ASx-6-SN0
subnet 2001:db8::/96
object-group network ASx
network-object object ASx-4-SN0
network-object object ASx-6-SN0
