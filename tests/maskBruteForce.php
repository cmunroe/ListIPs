<?php

require "../ListIPs/ListIPs.php";

$ListIPs = new \ListIPs\core;

// All Possible IPs.
$maxIP = 4294967296;
// Add one extra for fun!
$maxIP++;

// Loop through all ips!
for($i = 0; $i < $maxIP; $i++){

    // Generate our IP.
    $ip = long2ip($i);

    // Generate our result.
    $result = $ListIPs->mask2cidr($ip);

    // If the result doesn't equal false.
    if($result !== false){

        // We should have a subnet mask.
        echo "Passed: " . $ip . "\n";
    }

}