<?php

// Load tests.
use PHPUnit\Framework\TestCase;

class maskTest extends TestCase{

    public function testMasks(){

        // Let's get our class.
        $ListIPS = new \ListIPs\core;

        // Create an array to check
        $checkMe = array();

        // Input [values] = result.
        $checkme['128.0.0.0'] = 1;
        $checkme['192.0.0.0'] = 2;
        $checkme['224.0.0.0'] = 3;
        $checkme['240.0.0.0'] = 4;
        $checkme['248.0.0.0'] = 5;
        $checkme['252.0.0.0'] = 6;
        $checkme['254.0.0.0'] = 7;
        $checkme['255.0.0.0'] = 8;
        $checkme['255.128.0.0'] = 9;
        $checkme['192.168.1.1'] = false;
        $checkme['test'] = false;
        $checkme[' 255.255.0.0 '] = 16;
        $checkme['255.255.255.224'] = 27;


        // Running each check.
        foreach($checkme as $key => $value){

            // Checking each array.
            $this->assertEquals($ListIPS->mask2cidr($key), $value);

        }

    }

    public function testInvalid(){
        // Let's get our class.
        $ListIPS = new \ListIPs\core;
                
        $this->assertFalse($ListIPS->mask2cidr("0.0.0.1"));
        
    }

}