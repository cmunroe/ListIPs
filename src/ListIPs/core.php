<?php

// Creating our namespace.
namespace ListIPs;

/**
 * Core LibreNMS function.
 */
class core {

    // Set our default entry array as public.
    protected $ips;

    // Define the place where we put built files.
    protected $list;

    // define our list type.
    protected $listType;

    /**
     * Constructor.
     * 
     * Does nothing special.
     */
    public function __construct()
    {

        // Set our list to being a array.
        $this->ips = array();

        // Initialize list.
        $this->listinit();

    }

    /**
     * Transform a IPv4 cidr to a subnet mask.
     * 
     * 24 -> 255.255.255.0
     * 16 -> 255.255.0.0
     *
     * @param string $cidr CIDR 
     * @return string subnet mask.
     */
    public function cidr2mask(string $cidr)
    {

        // Trimming
        $cidr = trim($cidr);

        // Make sure it is numeric.
        if(!is_numeric($cidr)){

            return false;

        }
        // Make sure we have a CIDR within tollerance.
        if ($cidr < 0 || $cidr > 32) {

            // We have a CIDR that is out of bounds.

            // return false for bad cidr.
            return false;
        }

        // Set our built string
        $mask = null;

        // Let's get our numeric value of the CIDR.
        $ipLen = pow(2, (32 - $cidr)) - 1;

        // convert to address.
        $reverseMask = long2ip($ipLen);

        // Explode and make into an array.
        $reverseMask = explode(".", $reverseMask);

        // loop through our array.
        for ($i = 0; $i < 4; $i++) {

            // Effectively reverse array.
            $mask .= abs($reverseMask[$i] - 255);

            // Make sure we don't add an extra "." at the end.
            if ($i < 3) {
                // Add "." inbetween sections.
                $mask .= ".";

            }

        }

        // return.
        return $mask;

    }

    /**
     * Transform a IPv4 subnet mask into CIDR format.
     *
     * @param string $mask subnet mask 255.255.255.0
     * @return int cidr
     */
    public function mask2cidr(string $mask)
    {

        // Let's remove spaces if they exist.
        $mask = trim($mask);

        // Let's verify we have a mask, aka a special IP Address.
        if (!filter_var($mask, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {

            // We don't have a valid IP address.

            return false;

        }

        // Let's explode into an array.
        // $mask = [255, 255, 255, 0];
        $mask = explode(".", $mask);

        // create a ip binary!
        $ipBinary = null;

        // Create Reverse.
        $ipReverse = null;

        // loop through masks octets
        foreach ($mask as $octet) {

            // append to $ipBinary.
            $ipBinary .= sprintf("%08d", decbin($octet));

            // Append to reverse.
            $ipReverse .= abs($octet - 255) . ".";

        }

        // See if we follow the standard for a mask.
        if (strpos($ipBinary, "01") !== false) {

            // we have a regular IP address.
            return false;

        }

        // Remove trailing period from foreach loop.
        $ipReverse = rtrim($ipReverse, ".");

        // calculate long version of ip
        $ipLen = ip2long($ipReverse) + 1;

        // Reverse the power.
        $cidr = floor(log($ipLen) / log(2));

        // Reverse cidr.
        $cidr = 32 - $cidr;

        // return finished CIDR.
        return $cidr;

    }

    
    /**
     * Import IP list into class.
     *
     * @param [string/array] $items List of ips with cidr or subnet mask you wish to import.
     * @return this chain.
     */
    public function import($items)
    {

        // Determine if we have a string. For A single entry.
        if (is_string($items)) {

            // Determine if this is a txt file!?
            if (strpos($items, "\n") !== false) {

                $items = explode("\n", $items);

                // This should get caught by the array
                // section in the next bit.
            } else {

                // Likely is just a single.
                // Import into list.
                $this->ipsBuilder($items);

            }

        }

        // Determine if we have an array of items.
        if (is_array($items)) {

            // Loop through array.
            foreach ($items as $item) {

                // Import into list.
                $this->ipsBuilder($item);

            }

        }

        // chaining
        return $this;
    }

    /**
     * Build out our ip list.
     * 
     * Should be exectued through import();
     *
     * @param [array/string] $item ip list.
     * @return this chain.
     */
    protected function ipsBuilder($item)
    {

        // Trimming!
        $item = trim($item);

        // Determine if we have a CIDR on the string.
        if (strpos($item, "/") !== false) {

            $item = explode("/", $item);

            // Trimming!
            $item[0] = trim($item[0]);
            $item[1] = trim($item[1]);

            // Make sre we have a valid IP
            if (!filter_var($item[0], FILTER_VALIDATE_IP)) {

                // We don't have a valid ip
                return false;
            }

            // check if we have a CIDR
            if (is_numeric($item['1'])) {

                //Determine if we have an IPv6.
                if(filter_var($item[0], FILTER_VALIDATE_IP, FILTER_FLAG_IPV6)){

                    // Append to the list. IPv6 doesn't support cidr2mask.
                    $this->ipsAppend($item[0], $this->cidr2mask6($item[1]), $item[1]);

                }

                else{

                    // We have an IPv4, append to our list.
                    $this->ipsAppend($item[0], $this->cidr2mask($item[1]), $item[1]);

                }
                
            }

            // Determine if we have a subnet mask
            if (filter_var($item[1], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {

                // Determine if a valid subnet.
                if ($this->mask2cidr($item[1])) {

                    // Append to list.
                    $this->ipsAppend($item[0], $item[1], $this->mask2cidr($item[1]));

                }

            }

        }

        // Determine if we have a range.
        elseif (strpos($item, "-") !== false) {

            // found a range... exploding.
            $item = explode("-", $item);

            // Trimming
            $item[0] = trim($item[0]);
            $item[1] = trim($item[1]);

            // Make sure both are IPs.
            if (filter_var($item[0], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4) && filter_var($item[1], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {

                // Turning into numerics.
                $item[0] = ip2long($item[0]);
                $item[1] = ip2long($item[1]);

                // Finding the bigger of the two.
                if ($item[0] < $item[1]) {

                    // Setting the start and end points.
                    $ipStart = $item[0];
                    $ipEnd = $item[1];

                } 
                
                else {

                    // Setting the start and end points.
                    $ipStart = $item[1];
                    $ipEnd = $item[0];

                }

                // Loop until we hit the end point.
                for ($i = $ipStart; $i <= $ipEnd; $i++) {

                    // append to list.
                    $this->ipsAppend(long2ip($i));

                }

            }

        }

        // We have just a single.
        else {

            // Append single IP to
            $this->ipsAppend($item);

        }

        return $this;

    }

    /**
     * Append IP to list.
     *
     * @param string $ip address
     * @param string $subnet Mask
     * @param string $cidr Mask 
     * @return boolean true if good IP, false if bad IP.
     */
    protected function ipsAppend(string $ip, string $subnet = null, string $cidr = null)
    {

        // Determine our type of IP
        if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {

            // Found IPv4
            $type = 4;

            // See if subnet is set.
            if ($subnet === null) {

                $subnet = "255.255.255.255";

            }

            // See if cidr is set.
            if ($cidr === null) {

                $cidr = 32;
            }

        }

        elseif (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6)) {

            // Found IPv6
            $type = 6;

            // See if subnet is set.
            if ($subnet === null) {

                $subnet = $this->cidr2mask6(128);

            }

            // See if cidr is set.
            if ($cidr === null) {

                $cidr = 128;
            }

        }

        else{

            // We didn't validate either an IPv4 -or- IPv6, dropping.
            return false;

        }

        // create a new array
        $this->ips[] = array(
            'ip' => $ip,
            'subnetMask' => $subnet,
            'cidr' => $cidr,
            'type' => $type,
        );

        return true;

    }

    /**
     * Dump IP list.
     *
     * @return object ip list currently in class.
     */
    public function dump()
    {

        // return the list.
        return $this->ips;

    }

    /**
     * Reset our stored ip list to null.
     *
     * @return this chain.
     */
    public function delete()
    {

        // Delete our list.
        unset($this->ips);

        unset($this->list);

        // Recreate it as an array.
        $this->ips = array();

        $this->list = array();

        // Chaining
        return $this;

    }

    /**
     * Export our formatted list in Json.
     *
     * @param string $name Add a name variable to the JSON output.
     * @return string json list.
     */
    public function json(string $name = null){


        // Set our array.
        $array = array();

        // Specify our name attribute if set.
        if($name !== null){

            // Set our name key.
            $array['name'] = $name;
            
        }
        
        // Define our array type;
        $array['type'] = $this->listType;

        // Define our list.
        $array['list'] = $this->list;

        // create json

        $json = json_encode($array, JSON_PRETTY_PRINT);

        // echo our response.
        return $json;

    }

    /**
     * Return our formatted list as a text file.
     *
     * @return string text.
     */
    public function text(){

        // Create file to return.
        $text = null;

        // Loop through...
        foreach($this->list as $item){

            $text .= $item . "\n";

        }

        // return text.
        return $text;

    }
    
    /**
     * Return our formatted list as a html text file.
     *
     * @return string text.
     */
    public function htmlText(){

        // Create file to return.
        $text = null;

        // Loop through...
        foreach($this->list as $item){

            $text .= $item . "<br />";

        }

        // return text.
        return $text;

    }

    /**
     * Set our list type.
     *
     * @param string $listType set our list type.
     * @return void.
     */
    protected function listinit(string $listType = null){

        // Set our list type.
        $this->listType = $listType;

        // Delete old formatted lists.
        unset($this->list);

        // make it into an array.
        $this->list = array();

    }

    /**
     * Remove all IPv6 Addresses.
     *
     * @return this chain.
     */
    public function limit4(){

        // Get our ips
        $ips = $this->ips;

        // Clean up.
        $this->delete();

        foreach($ips as $ip){

            // Check to see if this is an IPv4.
            if($ip['type'] == 4){

                // Found an IPv4, import.
                $this->ips[] = $ip;
             
            }

        }

        // Add chaining
        return $this;

    }

    
    /**
     * Remove all IPv4 addresses from the list.
     *
     * @return this chain.
     */
    public function limit6(){

        // Get our ips
        $ips = $this->ips;

        // Clean up.
        $this->delete();

        foreach($ips as $ip){

            // Check to see if this is an IPv4.
            if($ip['type'] == 6){

                // Found an IPv4, import.
                $this->ips[] = $ip;
             
            }

        }

        // Add chaining
        return $this;

    }

    /**
     * Transform a IPv6 CIDR to and IPv6 subnet mask.
     *
     * @param string $cidr
     * @return string IPv6 Subnet Mask
     */
    public function cidr2mask6(string $cidr){

        // Trimming
        $cidr = trim($cidr);

        // Make sure it is numeric.
        if(!is_numeric($cidr)){

            return false;

        }

        // Check to make sure we are in bounds.
        if($cidr < 0 || $cidr > 128){
        
            // WE are not in the bounds for an 
            // IPv6 subnet.
        
            // return false.
            return false;
        
        }
        
        // create return
        $subnetArray = null;
        
        // Build decimal.
        for($i = 0; $i < 8; $i++){
        
            // Set up our scope.
            $scope = null;
        
            // Build out the binary mask.
            for($ii = 0; $ii < 16; $ii++){
        
                // Check to see if we have a cidr remaining.
                if($cidr > 0){
        
                    // Append to scope, and remove from cidr.
                    $scope .= 1;
                    $cidr--;
        
                }
                else{
                    
                    // Nothing left, append zero
                    $scope .= 0;
                }
        
            }
        
            // Attach to array
            $subnetArray[$i] =  dechex(bindec($scope));
        
        }
        
        // create our subnetmask
        $subnetMask = null;
        
        // Loop through the returned array
        foreach($subnetArray as $key => $section){
        
            // See if the section isn't a zero.
            if($section !== '0'){
        
                // Append to subnetmask
                $subnetMask .= $section;
        
                // Add our ":" delimiter if not at the end.
                if($key < 7){
        
                    $subnetMask .= ":";
        
                }
        
            }
            else{
        
                // We are done, append the :: delimiter.
                $subnetMask .= ":";
        
                // Return as we are finished.
                return $subnetMask;
        
            }
        
        }
        
        // We are also finished.
        return $subnetMask;
        
    }

}
