<?php

// Creating our namespace.
namespace ListIPs;

/**
 * Cisco List.
 */
class cisco extends core {
    
    /**
     * Create a cisco black list.
     *
     * @param string name set the name of this list.
     * @return this chain.
     */
    public function cisco(string $name = null){

        // Notes!! Do to how this list is formatted.
        // We will create two seperate arrays.
        // One will contain our objects, and then subnets.
        // The next will contain our network-objects. 
        // We will then combine them to make
        // One solid list to be used for our list.

        // initialize formatted list.
        $this->listinit("cisco");

        // Make sure we have a name.
        if($name === null){

            // Set our name to generic.
            $name = "myBlocklist";

        }

        // Set counters.
        $v4Counter = 0;
        $v6Counter = 0;

        // create post and pre array.
        $pre = array();
        $post = array();

        // create inital post bit.
        $post[] = "object-group network " . $name;

        // Loop through IP List.
        foreach($this->ips as $item){

            // IPv4
            if($item['type'] == 4){

                // append this section to our pre list.
                $pre[] = "object network " . $name . "-4-SN" . $v4Counter;
                $pre[] = "subnet " . $item['ip'] . " " . $item['subnetMask'];

                // append to post section.
                $post[] = "network-object object " . $name . "-4-SN" . $v4Counter;

                // increase counter.
                $v4Counter++;

            }

            // IPv6
            if($item['type'] == 6){

                // append this to our pre list.
                $pre[] = "object network " . $name . "-6-SN" . $v6Counter;
                $pre[] = "subnet " . $item['ip'] . "/" . $item['cidr'];

                // append to post section.
                $post[] = "network-object object " . $name . "-6-SN" . $v6Counter;

                // Increase Counter.
                $v6Counter++;

            }

        }

        // merge pre and post into the list.
        $this->list = array_merge($pre, $post);

        // Chaining.
        return $this;

    }

}