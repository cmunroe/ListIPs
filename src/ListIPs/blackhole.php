<?php

// Creating our namespace.
namespace ListIPs;

/**
 * Black Hole class for Linux Blackholes.
 */
class blackhole extends core {

    /**
     * Create a black hole ip list.
     *
     * @return this chain.
     */
    public function blackhole(){

        // initialize formatted list.
        $this->listinit("ip blackhole");

        // Loop through IP List.
        foreach($this->ips as $item){

            // Append to our formmated list.
            $this->list[] = "ip route add blackhole " . $item['ip'] . "/" . $item['cidr'];

        }

        // Chaining.
        return $this;

    }

    /**
     * Create a black hole ip delete list. 
     *
     * @return this chain.
     */
    public function blackholeDelete(){

        // initialize formatted list.
        $this->listinit("ip blackhole remove");

        // Loop through IP List.
        foreach($this->ips as $item){

            // Append to our formmated list.
            $this->list[] = "ip route del blackhole " . $item['ip'] . "/" . $item['cidr'];

        }

        // Chaining.
        return $this;

    }

}