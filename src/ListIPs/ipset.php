<?php

// Creating our namespace.
namespace ListIPs;

/**
 * Ipset Class.
 */
class ipset extends core {

    /**
     * Undocumented function
     *
     * @param string $alias for marking the ipset records.
     * @return this chain.
     */
    public function ipset(string $alias = null){

        // Make sure $alias is defined.
        if($alias === null){

            $alias = "myBlackhole";

        }

        // initialize formatted list.
        $this->listinit("ipset");

        // Initialize IPv4 Ipset.
        $this->list[] = "ipset -N " . $alias . "-4 hash:net family inet";

        // Initialize IPv6 Ipset.
        $this->list[] = "ipset -N " . $alias . "-6 hash:net family inet6";

        // Loop through IP List.
        foreach($this->ips as $item){

            // IPv4
            if($item['type'] == 4){

                // Append to our formmated list.
                $this->list[] = "ipset -A " . $alias . "-4 " . $item['ip'] . "/" . $item['cidr'];

            }

            // IPv6
            if($item['type'] == 6){

                // Append to our formatted list.
                $this->list[] = "ipset -A " . $alias . "-6 " . $item['ip'] . "/" . $item['cidr'];

            }

        }

        // Add our IPv4 iptables rule.
        $this->list[] = "iptables -A INPUT -m set --match-set " . $alias ."-4 src -j DROP";

        // Add our IPv6 iptables rule.
        $this->list[] = "ip6tables -A INPUT -m set --match-set " . $alias ."-6 src -j DROP";

        // Chaining.
        return $this;
        
    }

}
