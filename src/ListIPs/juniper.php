<?php

// Creating our namespace.
namespace ListIPs;

/**
 * Juniper Class
 */
class juniper extends core {

    /**
     * Create Juniper List.
     *
     * @param string $name that we want to attach to the juniper rule set.
     * @return this chain.
     */
    public function juniper(string $name = null){

        // Create formatted list for Juniper. 
        // Requested by Ryan Arp.

        // initialize formatted list.
        $this->listinit("juniper");

        // Make sure we have a name.
        if($name === null){

            // Set our name to generic.
            $name = "myBlocklist";

        }

        
        // Loop through IP List.
        foreach($this->ips as $item){

            // IPv4
            if($item['type'] == 4){

                // Append to our formmated list.
                $this->list[] = 'set policy-options prefix-list ' . $name . 'v4 ' . $item['ip'] . "/" . $item['cidr'];

            }

            
            // IPv6
            if($item['type'] == 6){

                // Append to our formmated list.
                $this->list[] = 'set policy-options prefix-list ' . $name . 'v6 ' . $item['ip'] . "/" . $item['cidr'];
                
            }

        }

        // Chaining.
        return $this;

    }

}