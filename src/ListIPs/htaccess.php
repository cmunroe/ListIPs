<?php

// Creating our namespace.
namespace ListIPs;

/**
 * HTAccess List.
 * 
 * For Apache.
 */
class htaccess extends core {

    /**
     * Create a HTAccess list for Apache.
     *
     * @return this chain.
     */
    public function htaccess(){

        // initialize formatted list.
        $this->listinit("htaccess");

        // prepend top.
        $this->list[] = "Order Deny,Allow";

        // Loop through IP List.
        foreach($this->ips as $item){

            // Append to our formmated list.
            $this->list[] = "Deny from " . $item['ip'] . "/" . $item['cidr'];

        }

        // Chaining.
        return $this;

    }

}