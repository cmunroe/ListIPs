<?php

// Creating our namespace.
namespace ListIPs;

/**
 * Raw class
 */
class raw extends core {

    /**
     * Create a RAW list.
     *
     * @return this chain.
     */
    public function raw(){

        // initialize formatted list.
        $this->listinit("raw");

        // Raw is easy peasy lemon squeezy!
        $this->list = $this->ips;

        // Chaining.
        return $this;

    }

}