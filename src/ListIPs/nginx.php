<?php

// Creating our namespace.
namespace ListIPs;

/**
 * Nginx Class.
 */
class nginx extends core {

    /**
     * Create an nginx block list.
     *
     * @return this chain.
     */
    public function nginx(){

        // initialize formatted list.
        $this->listinit("nginx");

        // Loop through IP List.
        foreach($this->ips as $item){

            // Append to our formmated list.
            $this->list[] = "deny " . $item['ip'] . "/" . $item['cidr'] . ";";

        }

        // Chaining.
        return $this;

    }

}