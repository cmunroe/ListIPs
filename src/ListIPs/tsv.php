<?php

// Creating our namespace.
namespace ListIPs;

/**
 * TSV Class.
 */
class tsv extends core {
    
    /**
     * Create TSV list.
     *
     * @return this chain.
     */
    public function tsv(){

        // initialize formatted list
        $this->listinit("tsv");

        // Create our title blocks.
        $this->list[] = "IP Address\tSubnet Mask\tCIDR\tType";

        // Loop through and append to list.
        foreach($this->ips as $item){

            // See if this is IPV6
            if($item['type'] === 6){

                // Append to our list.
                $this->list[] = $item['ip'] . "\t" .  "FALSE" . "\t" . $item['cidr'] . "\t" . $item['type'];

            }
            else{
                
                // Append to our list.
                $this->list[] = $item['ip'] . "\t" .  $item['subnetMask'] . "\t" . $item['cidr'] . "\t" . $item['type'];


            }

        }
        
        // chaining support.
        return $this;

    }

}