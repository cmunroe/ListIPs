<?php

// Creating our namespace.
namespace ListIPs;

/**
 * CSV Text List.
 */
class csv extends core {
    
    /**
     * Create CSV.
     *
     * @return this chain.
     */
    public function csv(){

        // initialize formatted list
        $this->listinit("csv");

        // Create our title blocks.
        $this->list[] = "IP Address,Subnet Mask,CIDR,Type";

        // Loop through and append to list.
        foreach($this->ips as $item){

            // See if we have type 6
            if($item['type'] === 6){

                // Append to our list.
                $this->list[] = $item['ip'] . "," .  "FALSE" . "," . $item['cidr'] . "," . $item['type'];

            }
            else{

                // Append to our list.
                $this->list[] = $item['ip'] . "," .  $item['subnetMask'] . "," . $item['cidr'] . "," . $item['type'];

            }

        }
        
        // chaining support.
        return $this;

    }

}