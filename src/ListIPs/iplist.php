<?php

// Creating our namespace.
namespace ListIPs;

/**
 * IPlist Class
 */
class iplist extends core {

    /**
     * Create a list of ips.
     * 127.0.0.1/8
     *
     * @return this chain.
     */
    public function ipList(){

        // initialize formatted list.
        $this->listinit("ipList");

        // Loop through IP List.
        foreach($this->ips as $item){

            // Append to our formmated list.
            $this->list[] = $item['ip'] . "/" . $item['cidr'];

        }

        // Chaining.
        return $this;

    }

}