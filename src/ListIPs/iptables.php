<?php

// Creating our namespace.
namespace ListIPs;

/**
 * iptables class.
 */
class iptables extends core {

    /**
     * Create iptables list for linux.
     *
     * @return this chain.
     */
    public function iptables(){

        // initialize formatted list.
        $this->listinit("iptables");

        // Loop through IP List.
        foreach($this->ips as $item){

            // IPv4
            if($item['type'] == 4){

                // Append to our formatted list.
                $this->list[] = "iptables -A INPUT -s " . $item['ip'] . "/" . $item['cidr'] . " -j DROP";

            }

            // IPv6
            if($item['type'] == 6){

                // Append to our formatted list.
                $this->list[] = "ip6tables -A INPUT -s " . $item['ip'] . "/" . $item['cidr'] . " -j DROP";

            }

        }

        // Chaining.
        return $this;

    }

}